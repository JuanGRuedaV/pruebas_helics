# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 10:24:16 2022

@author: Juan
"""
import json
import os

# =============================================================================
# Removing log files
# =============================================================================
current_Path = os.getcwd().replace('\\', '/')
log_Path = current_Path+'/Helics' 

dirs = os.listdir(log_Path)
team = []
for file in dirs:
    check = file.split(".")
    if len(check) == 1:
        pass
    else:
        if check[1] == 'log':
            # print(check)
            os.remove(str(log_Path)+'/'+str(file))