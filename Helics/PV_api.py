from datetime import datetime
import helics as h
import os
import sys

# # # Loading Libraries #
sys.path.append(os.getcwd().replace('\\', '/')+'/classHelics')
sys.path.append(os.getcwd().replace('\\', '/')+'/Models')

from classHelics.classFederate import Federate
from Models.PV_model import PV

# # # Checking broker connection #
helicsversion = h.helicsGetVersion()
print("PV: Helics version = {}".format(helicsversion))

day = 0
count = 0
for i in range(len(sys.argv)):
    count = i+1
    if count == len(sys.argv):
        break
    else :
        ent = str(sys.argv[i+1]).split(':')
        if ent[0] == 'sim_start':
            START = datetime.fromtimestamp(int(ent[1]))
        if ent[0] == 'days':
            day = ent[1]
        if ent[0] == 'name':
            name = ent[1]
        if ent[0] == 'datafile':
            datafile = ent[1]

# # # Loading Houses information #
current_Path = os.getcwd().replace('\\', '/')
files_Path = current_Path+'/Files/'
os.chdir(files_Path)

# file = datafile
# date = '2014-01-01 00:00:00'

x = PV()
x.Inicio(datafile, str(START))

# # # Back to main path #
main_Path = os.path.normpath(os.getcwd() + os.sep + os.pardir).replace('\\', '/')
os.chdir(main_Path)

day = int(day)*24*60

# # # Loading JSON info #
current_Path = os.getcwd().replace('\\', '/')
jsons_Path = current_Path+'/Jsons/'
print(jsons_Path)
os.chdir(jsons_Path)

json_file = name

# # # Creating federate #

fed = Federate()
fed.create_federate(json_file)
print("Creados los federates")
pub_count = h.helicsFederateGetPublicationCount(fed.vfed)

pubid = {}
if pub_count == 0:
    pass
else:
    for i in range(0, pub_count):
        pubid[i] = h.helicsFederateGetPublicationByIndex(fed.vfed, i)
        pub_name = h.helicsPublicationGetName(pubid[i])

# # # Starting Federates #
fed.start_async()
h.helicsFederateEnterExecutingModeComplete(fed.vfed)
print("Iniciando los federates")

# # # Executing loop #
step = -1
data = x.Crear()
print('los datos son:', len(data))
while step < day:
    step = fed.getCurrentTime()
    print(step)
    print("Current Time =" ,step)
    if pub_count == 0:
        pass
    else:
        sendData = data[int(step)]
        print(sendData)
        for i in range(pub_count):
            txt = str(sendData)
            fed.publishString(pubid[i], txt)
            # fed[i].publish(fed[i].pub[0], value[i])
            print("PV: Sending value =" ,txt, "at time =", step , " to Pypower")
    next = fed.getNextGrantedTime()
    print("Next Time =" ,next)

# # # Executing finish #
fed.destroy()
print("federates finalized")
h.helicsCloseLibrary()