# -*- coding: utf-8 -*-
"""
Created on Thu Apr 22 23:01:39 2021

@author: Juan
"""
import model_pypower as mp

class PypowerModel():
    # Pypower simulator
    def start(self, gridfile, data, files_path, funct):
        ppcs = []
        # self.gridfile = 'demo_lv_grid.json'
        self.gridfile = gridfile
        self.data = data
        self.address = files_path+self.gridfile
        self.funct = funct
        ppc, entities = mp.load_case(self.address, grid_idx=0, sheetnames=None)
    
        grids = []
        _entities = {}
        grid_idx = len(ppcs)
        ppcs.append(ppc)
    
        children = []
        for eid, attrs in sorted(entities.items()):
            assert eid not in _entities
            _entities[eid] = attrs
        
            # We'll only add relations from branches to nodes (and not from
            # nodes to branches) because this is sufficient for mosaik to
            # build the entity graph.
            relations = []
            if attrs['etype'] in ['Transformer', 'Branch']:
                relations = attrs['related']
        
            children.append({
                'eid': eid,
                'type': attrs['etype'],
                'rel': relations,
            })
        
        grids.append({
            'eid': mp.make_eid('grid', grid_idx),
            'type': 'Grid',
            'rel': [],
            'children': children,
        })
    
        juan = []
        # hola = data[1]['P']
        for i in range(len(self.data)):
            salida = mp.case_for_eid(self.data[i]['eid'],ppcs)
            # juan.append(salida)
            idx = _entities[self.data[i]['eid']]['idx']
            etype = _entities[self.data[i]['eid']]['etype']
            static = _entities[self.data[i]['eid']]['static']
            mp.set_inputs(ppc, etype, idx, self.data[i], static)
            juan.append(ppc)
        
        # print('las entities son:', _entities)
        # Generando la información del flujo de potencia - Los datos a enviar
        res = []
        for i in range(len(self.data)):
            res.append(mp.perform_powerflow(juan[i]))
        _cache = mp.get_cache_entries(res, _entities)
        # return _cache
        datosFlow_branches = []
        # print(type(_cache))
        for k,v in _cache.items():
            if k.split('-')[1].split('_')[0] == 'branch':
                values = v
                values['eid'] = k
                datosFlow_branches.append(values)
        # print(branches)
        # _cache = model.get_cache_entries(res, _entities)
        # print(_cache)
        """ # Seleccionar los el eid de los nodos"""
        nodos = []
        for i in range(len(children)):
            if children[i]['type'] == 'PQBus':
                nodos.append(children[i]['eid'])
        # print(nodos)
        
        """ # Seleccionar la información para enviar a la base de datos"""
        datosFlow_nodos = []
        for key in _cache:
            for i in range(len(nodos)):
                if key == nodos[i]:
                    val = _cache[key]
                    val['eid'] = key
                    datosFlow_nodos.append(val)
                    i = i+1
                else:
                    i = i+1
        
        if self.funct == 'nodes':
            datosFlow = datosFlow_nodos
        elif self.funct == 'branches':
            datosFlow = datosFlow_branches
        
        return datosFlow, children

# x =  PypowerModel()

# path = 'C:/Users/Juan/Dropbox/Master_of_Science/Thesis/FRAMEWORKS/Case_study_0/Helics/Files/'
# gridfile = 'demo_lv_grid.json'
# hhsim_data = [{'eid': '0-node_d1', 'P': 330.67},
#               {'eid': '0-node_d4', 'P': 200},
#               {'eid': '0-node_c12', 'P': 310.67},
#               {'eid': '0-node_d10', 'P': 500.67}]

# salida, otro = x.start(gridfile, hhsim_data, path, 'nodes')
# for i in range(len(salida)):
#     print({'Vm':salida[i]['Vm'], 'eid':salida[i]['eid']})
# # print(salida)

# # pypower_data = [{'P': 0.0, 'Q': 0.0, 'Vm': 229.99999819912298, 'Va': -1.7102604969203485e-06, 'eid': '0-node_a1'}, {'P': 0.0, 'Q': 0.0, 'Vm': 229.99999819912298, 'Va': -1.7102604942337968e-06, 'eid': '0-node_a2'}]