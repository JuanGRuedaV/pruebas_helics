# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 15:22:51 2021

@author: Juan
"""
import h5py
import os.path
import gzip
import ast

class HDF5():
    
    """Método inicio solo se utiliza para crear DB y los diferentes grupos"""
    def Inicio(self, dbname, data, tamaño=100000000000):
        """# DB file created"""
        self.name = dbname
        self.data = ast.literal_eval(data)
        self.tamaño = tamaño
        self.f = h5py.File(self.name, "a")
        
        # Revisar cuántos grupos hay que crear en la DB
        self.grp = []
        x = 'No'
        for i in range(len(self.data)):
            for k,v in self.data[i].items():
                if not len(self.grp):
                    if k == 'group':
                        self.grp.append(v)
                    else:
                        pass
                else:
                    if k == 'group':
                        if any(v in s for s in self.grp):
                            pass
                        else:
                            self.grp.append(v)
        
        # Creando los grupos de la DB
        for i in range(len(self.grp)):
            grupos = list(self.f.keys())
            if any(self.grp[i] in s for s in grupos):
                pass
            else:
                self.f.create_group(self.grp[i]) # Ésta línea solo se debe ejecutar una vez
        
        # Revisar los eid que se deben crear en cada grupo de la DB
        self.grp_eid= []
        for i in range(len(self.data)):
            for k,v in self.data[i].items():
                if any(str(v) in s for s in self.grp):
                    for key,val in self.data[i].items():
                        if key == 'eid':
                            self.grp_eid.append({v:val})
                        else:
                            pass
        
                
        for i in range(len(self.grp_eid)):
            for k,v in self.grp_eid[i].items():
                subgrupos = list(self.f[str(k)])
                if any(v in s for s in subgrupos):
                    pass
                else:
                    self.f.create_dataset('/'+str(k)+'/'+str(v), (100000,self.tamaño), dtype='f8') # Ésta línea solo se debe ejecutar una vez
        self.f.close()
    
    """Método para escibir información en cualquier grupo y variable"""
    def write(self, dbname, entrada, fila):
        self.f = h5py.File(dbname, "a")
        self.fila = fila
        self.entrada = entrada
        
        print(self.f.name)
        print(list(self.f.keys()))
        
        # Revisar cuántos grupos hay que crear en la DB
        self.grp = []
        x = 'No'
        for i in range(len(self.entrada)):
            for k,v in self.entrada[i].items():
                if not len(self.grp):
                    if k == 'group':
                        self.grp.append(v)
                    else:
                        pass
                else:
                    pass
        
        # Revisar los eid que se deben crear en cada grupo de la DB
        self.grp_eid= []
        for i in range(len(self.entrada)):
            for k,v in self.entrada[i].items():
                if any(str(v) in s for s in self.grp):
                    for key,val in self.entrada[i].items():
                        if key == 'eid':
                            self.grp_eid.append({v:val})
                        else:
                            pass
        
        # # Saber cuales son las variables que están ingresando
        for i in range(len(self.entrada)):
            knt = 0
            var = []
            for k,v in self.entrada[i].items():
                if k != 'eid' and k != 'group':
                    var.append({k:knt})
                    knt += 1
                elif k == 'group':
                    grupo = v
                else:
                    pass
            # print(var)

        # # Revisión del grupo y eid para escribir datos en la DB
        for i in range(len(self.entrada)):
            for k,v in self.entrada[i].items():
                if not k == 'eid':
                    pass
                else:
                    for key,val in self.entrada[i].items():
                        if not key == 'group':
                            pass
                        else:
                            if any(str(v) in s[val] for s in self.grp_eid):
                                print(v,val)
                                # # Saber cuales son las variables que están ingresando
                                knt = 0
                                var = []
                                for ke,va in self.entrada[i].items():
                                    if ke != 'eid' and ke != 'group':
                                        var.append({ke:knt})
                                        knt += 1
                                    elif k == 'group':
                                        grupo = va
                                    else:
                                        pass
                                # print(var)
                                for n in range(len(var)):
                                    for ky,vl in var[n].items():
                                            self.hset = self.f['/'+str(val)+'/'+str(v)]
                                            self.hset[self.fila,vl] = self.entrada[i][ky]
                                            # print(ky,vl)
                                            # print(pypower_data[i][ky])
                                            print('-------------')
                                            
        self.f.close()

"""Pruebas"""
# db = HDF5()

# pypower_data = [{'P': 0.0, 'Q': 0.0, 'Vm': 229.99999819912298, 'Va': -1.7102604969203485e-06, 'eid': '0-node_a1'}, {'P': 0.0, 'Q': 0.0, 'Vm': 218, 'Va': 10, 'eid': '0-node_a2'}]
# # data = str([{'P': 0.0, 'Q': 0.0, 'Vm': 229.99999819912298, 'Va': -1.7102604969203485e-06, 'eid': '0-node_a1'}, {'P': 0.0, 'Q': 0.0, 'Vm': 229.99999819912298, 'Va': -1.7102604942337968e-06, 'eid': '0-node_a2'}])
# # pypower_data = [{'P': 0.0, 'Q': 0.0, 'Vm': 229.99999819912298, 'Va': -1.7102604969203485e-06}, {'P': 0.0, 'Q': 0.0, 'Vm': 229.99999819912298, 'Va': -1.7102604942337968e-06}]
# for i in range(len(pypower_data)):pypower_data[i]['group'] = 'Grid'
# data = str(pypower_data)
# hhsim_data = [{'eid': 'node_c4', 'P_from': 330.67, 'Q': 657, 'Vm': 230, 'group':'ResidentialLoads'},
#               {'eid': 'node_d4', 'P_from': 200, 'Q': 57, 'Vm': 120, 'group':'ResidentialLoads'},
#               {'eid': 'node_c3', 'P_from': 310.67, 'Q': 65, 'Vm': 120, 'group':'ResidentialLoads'},
#               {'eid': 'node_d3', 'P_from': 500.67, 'Q': 210, 'Vm': 230, 'group':'ResidentialLoads'}]
# data_hhsim = str(hhsim_data)

# new = [{'eid': '0-node_c5', 'Vm': 136.76, 'group': 'PV'}]

# # db.Inicio('betto.hdf5',data)
# # db.write('betto.hdf5', pypower_data, 0)

# db.Inicio('juan.hdf5',str(new))
# db.write('juan.hdf5', new, 0)