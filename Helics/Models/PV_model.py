# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 12:14:25 2021

@author: Juan
"""
import json
import pandas as pd
import arrow
import gzip


class PV():
    
    def Inicio(self, pvfile, start):
        """# Loading data to create entities"""
        self.pv = open(pvfile,"r")
        self.DATE_FORMAT = ['YYYY-MM-DD HH:mm', 'YYYY-MM-DD HH:mm:ss']
        self.START = start
    
    def Crear(self):
        self.data = []
        pv_start = arrow.get(self.START, self.DATE_FORMAT)
        # datafile = open(self.pv, "r")
        datafile = self.pv
        datafile.readline()
        next(datafile).strip()
        # Get attribute names and strip optional comments
        # attrs = next(datafile).strip().split(',')
        attrs = next(datafile)
        while attrs != ValueError:
            values = attrs.split(',')
            power = abs(float((values[1])))
            info = {'date':values[0],'P':power,'sim':'PV'}
            self.data.append(info)
            try:
                attrs = next(datafile)
            except StopIteration:
                break
        
        return self.data
