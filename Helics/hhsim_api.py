from datetime import datetime
import helics as h
import os
import gzip
import sys

# # # Loading Libraries #
sys.path.append(os.getcwd().replace('\\', '/')+'/classHelics')
sys.path.append(os.getcwd().replace('\\', '/')+'/Models')

from classHelics.classFederate import Federate
from Models.model_house import HouseModel

# # # Checking broker connection #
helicsversion = h.helicsGetVersion()
print("HouseHold: Helics version = {}".format(helicsversion))

day = 0
count = 0
for i in range(len(sys.argv)):
    count = i+1
    if count == len(sys.argv):
        break
    else :
        ent = str(sys.argv[i+1]).split(':')
        if ent[0] == 'sim_start':
            START = datetime.fromtimestamp(int(ent[1]))
        if ent[0] == 'profile_file':
            pf = ent[1]
        if ent[0] == 'grid_name':
            demo = ent[1]
        if ent[0] == 'days':
            day = ent[1]
        if ent[0] == 'name':
            name = ent[1]

# # # Loading Houses information #
current_Path = os.getcwd().replace('\\', '/')
files_Path = current_Path+'/Files/'
os.chdir(files_Path)
PROFILE_FILE = gzip.open(pf, 'rt')
juan = demo

# # # Back to main path #
main_Path = os.path.normpath(os.getcwd() + os.sep + os.pardir).replace('\\', '/')
os.chdir(main_Path)

x = HouseModel(PROFILE_FILE, juan)
n= len(x.get_houses())

print('esto es x:', x)


day = int(day)*24*60

# # # Loading JSON info #
current_Path = os.getcwd().replace('\\', '/')
jsons_Path = current_Path+'/Jsons/'
print(jsons_Path)
os.chdir(jsons_Path)

json_file = name

# # # # Back to main path #
# main_Path = os.path.normpath(os.getcwd() + os.sep + os.pardir).replace('\\', '/')
# os.chdir(main_Path)

fed = Federate()
fed.create_federate(json_file)
print("Creados los federates")
pub_count = h.helicsFederateGetPublicationCount(fed.vfed)
print('las pubs son', pub_count)

pubid = {}
if pub_count == 0:
    pass
else:
    for i in range(0, pub_count):
        pubid[i] = h.helicsFederateGetPublicationByIndex(fed.vfed, i)
        pub_name = h.helicsPublicationGetName(pubid[i])

# # # Starting Federates #
fed.start_async()
h.helicsFederateEnterExecutingModeComplete(fed.vfed)
print("Iniciado el Federate")
name = h.helicsFederateGetName(fed.vfed).split('_')[0] # Obteniendo el nombre del federate
print('el name es:',name)
# # # Executing loop #
step = -1
horacasa = 0
info = x.get_houses()
print('esta es la info=',info)
while step < day:
    if pub_count == 0:
        step = fed.getCurrentTime()
        print("Current Time =" ,step)
    else:
        for i in range(pub_count):
            if name == 'ResidentialLoads':
                sendData = x.get_random(horacasa)
                # print('tamaño',len(sendData))
                sendData[i]['sim'] = 'HouseHold'
                step = fed.getCurrentTime()
                print("Current Time =" ,step)
                txt = str(sendData[i])
                fed.publishString(pubid[i], txt)
                # fed.publish(pubid[i+n], value[i])
                print("Printer: Sending value =" ,txt, "at time =", step , " to DB")
            else:      
                sendData = info
                value = x.get(horacasa)
                step = fed.getCurrentTime()
                print("Current Time =" ,step)
                sendData[i]['P'] = value[i]
                sendData[i]['sim'] = 'HouseHold'
                txt = str(sendData[i])
                fed.publishString(pubid[i], txt)
                # fed.publish(pubid[i+n], value[i])
                print("Printer: Sending value =" ,txt, "at time =", step , " to DB")
    
    next = fed.getNextGrantedTime()
    horacasa = horacasa+15
    print("Next Time =" ,next)

# # # Executing finish #
fed.destroy()
print("federates finalized")
h.helicsCloseLibrary()