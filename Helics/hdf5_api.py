from datetime import datetime
import helics as h
import random
import json
import ast
import os
import arrow
import time
import sys

# # # Loading Libraries #
sys.path.append(os.getcwd().replace('\\', '/')+'/classHelics')
sys.path.append(os.getcwd().replace('\\', '/')+'/Models')

from classHelics.classFederate import Federate
from Models.hdf5_model import HDF5

# # # Checking broker connection #
helicsversion = h.helicsGetVersion()
print("HDF5: Helics version = {}".format(helicsversion))

day = 0
# conf = JsonConfig('Hdf5', of=1, dl=1)
n = 37
# timeadv= int(sys.argv[1])
# ent2 = str(sys.argv[2]).split(':')
# if ent2[0] == 'filename':
#     dbfile = ent2[1]

count = 0
for i in range(len(sys.argv)):
    # # print(sys.argv[i])
    # count = i+1
    # print('count',count)
    # print('len',len(sys.argv))
    # if count == len(sys.argv):
    #     break
    # else :
    ent = str(sys.argv[i]).split(':')
    print('la key es:',ent[0])
    if ent[0] == 'days':
        day = ent[1]
    if ent[0] == 'name':
        name = ent[1]
    if ent[0] == 'filename':
        dbname = ent[1]
        # print(dbname)
    if ent[0] == 'info':
        info = ent
        print('la info:',info)
    if ent[0] != 'days' and ent[0] != 'name' and ent[0] != 'filename' and ent[0] != 'info' and ent[0] != 'hdf5_api.py' :
        # print(ent[0])
        timeadv = int(ent[0])

### Take info needed to create the DB ###
data = []
# print(len(info))
for i in range(len(info)):
    if i == 0:
        pass
    else:
        y = str(info[i]).split(',')
        for i in range(len(y)):
            x = str(y[i]).replace("[{", "")
            x = str(x).replace("}]", "")
            x = str(x).replace("{", "")
            x = str(x).replace("}", "")
            data.append(x)

new_data = []
y = {}
t = 0
for i in range(len(data)):
    if t < 2:
        if i%2 == 0:
            y['group'] = data[i]
            t += 1
        else:
            y['eid'] = data[i]
            t +=1
        if t == 2:
            new_data.append(y)
            y = {}
            t = 0
# print('nuevos datos',new_data)
day = int(day)*24*60

# # # Creating groups and subgroups in DB #
db = HDF5()
# print(dbname)
# print(new_data)
# db.Inicio(dbname,str(new_data))

# # # Loading JSON info #
current_Path = os.getcwd().replace('\\', '/')
jsons_Path = current_Path+'/Jsons/'
# print(jsons_Path)
os.chdir(jsons_Path)

# print(name)
json_file = name

# # # Creating federate #
# conf.subRegister(n,'Pot_H_','House')
# conf.subRegister(1,'Power_flow_','Pypower')
fed = Federate()
fed.create_federate(json_file)
print("Creado Hdf5 Federate")

sub_count = h.helicsFederateGetInputCount(fed.vfed)
subid = {}
for i in range(0, sub_count):
    subid[i] = h.helicsFederateGetInputByIndex(fed.vfed, i)
    sub_name = h.helicsSubscriptionGetKey(subid[i])
# print(sub_name)
# print(len(subid))
# # # Starting Federates #
fed.start_async()
h.helicsFederateEnterExecutingModeComplete(fed.vfed)
print("Iniciado el Federate")
# # # Executing loop #
step = -1
fila = 0
wakeHouse = False
while step < day:
    step = fed.getCurrentTime()
    Houses_info = []
    print("Current Time =" ,step)
    total_data = []
    for i in range(len(subid)):
        very = fed.check_values_returned(step,subid[i])
        txt = fed.subscribeString(subid[i])
        res = ast.literal_eval(txt)
        new = []
        if type(res) == list:
            for i in range(len(res)):
                data = {}
                for k,v in res[i].items():
                    if k != 'sim':
                        data[k] = v
                    else:
                        data['group'] = v
                new.append(data)
        if type(res) == dict:
            data = {}
            for k,v in res.items():
                if k != 'sim':
                    data[k] = v
                else:
                    data['group'] = v
            new.append(data)
        # print('--------')
        # print(type(new),new)
        # print('*******')
    for t in range(len(new)):
        total_data.append(new[t])
    print('--------')
    print(total_data)
    print('*******')
    db.Inicio(dbname, str(total_data))
    db.write(dbname, total_data, fila)
    fila = fila+1
    #     # res = txt.split(',')
    #     res = ast.literal_eval(txt)
    # #     # Houses_info.append(res) Habilitar luego
    # #     if res['sim'] == 'PV':
    # #         dbPV= db.escribirPV(res, fila,'P')
    # #         fila = fila+1
    # #     elif res['sim'] == 'HouseHold':
    # #         Houses_info.append(res)
    # #         wakeHouse = True
    # # if wakeHouse == True:
    # #     dbDatos= db.escribir(Houses_info, fila, 'P')


    # print("DB: Receiving value =" ,res, "at time =", step , " from House_", i)
    avanzar = h.helicsFederateRequestTimeAdvance(fed.vfed,timeadv)
    # otrocheck = fed.check_values_returned(step,subid[1])
    # pf = fed.subscribeString(subid[1])
    # flujo_info = ast.literal_eval(pf)
    # # avanzar = h.helicsFederateRequestTimeAdvance(fed.vfed,60)
    # print(type(flujo_info))
    # print(flujo_info)
    # dbDatos= db.escribir(Houses_info, fila, 'P')
    # print(dbDatos)
    # dbFlujo= db.escribirflow(flujo_info, fila)
    # fila = fila+1
    # print(Houses_info)
    # # print(pf)
    next = fed.getNextGrantedTime()
    print("Next Time =" ,next)
# print(db.hset[0,:])
# print(db.f["/Nodes/0-node_a1"][0,:])
# # # Executing finish #
fed.destroy()
# print(Houses_info)
print("Federate finalized")
h.helicsCloseLibrary()