import helics as h

class Federate:
    
    def __init__(self):
        self.vfed = ""
    
    def __init__(self, core_type = "zmq", period=0, offset=0, delay=0, outdelay=0, time_delta = 0):
        self.__fedinfo = h.helicsCreateFederateInfo()
        h.helicsFederateInfoSetCoreName(self.__fedinfo, "")
        h.helicsFederateInfoSetCoreTypeFromString(self.__fedinfo, core_type)
        h.helicsFederateInfoSetCoreInitString(self.__fedinfo, "--federates=1")
        h.helicsFederateInfoSetTimeProperty(self.__fedinfo, h.helics_property_time_delta, time_delta)
        h.helicsFederateInfoSetTimeProperty(self.__fedinfo, h.helics_property_time_period, period)
        h.helicsFederateInfoSetTimeProperty(self.__fedinfo, h.helics_property_time_offset, offset)
        h.helicsFederateInfoSetTimeProperty(self.__fedinfo, h.helics_property_time_input_delay, delay)
        h.helicsFederateInfoSetTimeProperty(self.__fedinfo, h.helics_property_time_output_delay, outdelay)
        self.vfed = ""
        self.pub = []
        self.sub = []
    
    def create_federate(self, fedJson):
        # self.vfed = h.helicsCreateCombinationFederate(str(fedJson), self.__fedinfo)
        self.vfed = h.helicsCreateCombinationFederateFromConfig(fedJson)
        
    def getPub(self, pub_name):
        self.pubdata = h.helicsFederateGetPublication(self.vfed, pub_name)
        return self.pubdata
    
    def getSub(self, sub_name):
        self.subdata = h.helicsFederateGetSubscription(self.vfed, sub_name)
        return self.subdata
    
    def publish(self, target, in_val):
        self.doPub = h.helicsPublicationPublishDouble(target, in_val)
    
    def publishString(self, target, in_val):
        self.doPub = h.helicsPublicationPublishString(target, in_val)
    
    def addpublish(self, name, data_type):
        self.pub.append(h.helicsFederateRegisterGlobalTypePublication(self.vfed, name, data_type, ""))
    
    def subscribe(self, target):
        self.doSub = h.helicsInputGetDouble(target)
        return self.doSub
    
    def subscribeString(self, target):
        self.doSubString = h.helicsInputGetString(target)
        return self.doSubString
    
    def addsubscribe(self, target):
        self.sub.append(h.helicsFederateRegisterSubscription(self.vfed, target, ""))
    
    def advanceTimeDelta(self,delta):
        return h.helicsFederateRequestTimeAdvance(self.vfed, delta)
	
    def getCurrentTime(self):
        return h.helicsFederateGetCurrentTime(self.vfed)

    def getNextGrantedTime(self):
        return h.helicsFederateRequestNextStep(self.vfed)
    
    def destroy(self):
        # h.helicsFederateFinalize(self.vfed)
        h.helicsFederateDisconnect(self.vfed)
        h.helicsFederateFree(self.vfed)
#		print("Federate: Federate finalized")

    def start_async(self):
        h.helicsFederateEnterExecutingModeAsync(self.vfed)
    
    def start(self):
        h.helicsFederateEnterExecutingMode(self.vfed)
    
    def check_values_returned(self, i, subid):
        check = False
        if h.helicsInputIsUpdated(subid) == 0:
            check = False
            h.helicsFederateRequestTime(self.vfed, i)
        else:
            check = True