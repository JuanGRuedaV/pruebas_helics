from datetime import datetime
import helics as h
import os
import json
import ast
import sys
import re

# # # Loading Libraries #
sys.path.append(os.getcwd().replace('\\', '/')+'/classHelics')
sys.path.append(os.getcwd().replace('\\', '/')+'/Models')

from classHelics.classFederate import Federate
from pypower_model import PypowerModel

# # # Checking broker connection #
helicsversion = h.helicsGetVersion()
print("Pypower: Helics version = {}".format(helicsversion))

day = 0
count = 0
for i in range(len(sys.argv)):
    count = i+1
    if count == len(sys.argv):
        break
    else :
        ent = str(sys.argv[i+1]).split(':')
        if ent[0] == 'name':
            name = ent[1]
        if ent[0] == 'gridfile':
            gridfile = ent[1]
        if ent[0] != 'name' and ent[0] != 'gridfile' and ent[0] != 'days':
            timeadv = int(ent[0])
        if ent[0] == 'days':
            day = ent[1]

day = int(day)*24*60

# # # Loading Grid information #
# gridfile = 'demo_lv_grid.json'
grid = PypowerModel()

# # # Loading JSON info #
current_Path = os.getcwd().replace('\\', '/')
jsons_Path = current_Path+'/Jsons/'
print(jsons_Path)
os.chdir(jsons_Path)

json_file = name

# # # Creating federate #
fed = Federate()
fed.create_federate(json_file)
print("Creado Pypower Federate")

# # # Back to main path #
main_Path = os.path.normpath(os.getcwd() + os.sep + os.pardir).replace('\\', '/')
os.chdir(main_Path)

sub_count = h.helicsFederateGetInputCount(fed.vfed)
pub_count = h.helicsFederateGetPublicationCount(fed.vfed)

subid = {}
subid_key = []
if sub_count == 0:
    pass
else:
    for i in range(0, sub_count):
        subid[i] = h.helicsFederateGetInputByIndex(fed.vfed, i)
        sub_name = h.helicsSubscriptionGetKey(subid[i])
        subid_key.append(sub_name)

pubid = {}
pubid_key = []
if pub_count == 0:
    pass
else:
    for i in range(0, pub_count):
        pubid[i] = h.helicsFederateGetPublicationByIndex(fed.vfed, i)
        pub_name = h.helicsPublicationGetName(pubid[i])
        pubid_key.append(pub_name)

# # # Starting Federates #
fed.start_async()
h.helicsFederateEnterExecutingModeComplete(fed.vfed)
print("Iniciado el Federate")

# # # Executing loop #
step = -1
while step < day:
    step = fed.getCurrentTime()
    Houses_info = []
    print("Current Time =" ,step)
    if sub_count == 0:
        pass
    else: 
        for i in range(sub_count):
            power = 0
            very = fed.check_values_returned(step,subid[i])
            print('validate')
            txt = fed.subscribeString(subid[i])
            res = ast.literal_eval(txt)
            print(res)
            if res['sim'] == 'PV':
                power = int(res['P'])
            elif res['sim'] == 'HouseHold':
                info = res
        info['P'] = int(info['P'])+power
        Houses_info.append(info)
        # avanzar = h.helicsFederateRequestTimeAdvance(fed.vfed,timeadv)
        print(name, ": Receiving value =" ,info, "at time =", step , " from House_", i)
    
    # # # Loading Grid information #
    if pub_count == 0:
        pass
    else:
        current_Path = os.getcwd().replace('\\', '/')
        files_Path = current_Path+'/Files/'
        os.chdir(files_Path)

        # print('este es el dato BETTO:', gridfile)
        flow_nodes = grid.start(gridfile, Houses_info, files_Path, 'nodes')[0]
        flow_branches = grid.start(gridfile, Houses_info, files_Path, 'branches')[0]

        total_flow = []
        for i in range(len(flow_nodes)):
            total_flow.append(flow_nodes[i])
        for i in range(len(flow_branches)):
            total_flow.append(flow_branches[i])
        # print('el flujo total es:', total_flow)
        
        # # # Back to main path #
        main_Path = os.path.normpath(os.getcwd() + os.sep + os.pardir).replace('\\', '/')
        os.chdir(main_Path)
        name_group = str(name.split('_')[0])
        for i in range(pub_count):
            name_var = str(re.findall('(\d+|[A-Za-z]+)', pubid_key[i])[0]).split(name_group)[1]
            flujo = []
            for t in range(len(total_flow)):
                # print(name_group, name_var)
                if name_var in total_flow[t].keys():
                    # print(total_flow[t][name_var])
                    flujo.append({name_var:total_flow[t][name_var],
                            'eid':total_flow[t]['eid'],
                            'group':name_group})
                else:
                    pass
            pf = str(flujo)
            fed.publishString(pubid[i], pf)
        # print('tamaño_flujo:',total_flow)
        # pf = str(flujo)
        # fed.publishString(pubid[0], pf)
    
    avanzar = h.helicsFederateRequestTimeAdvance(fed.vfed,timeadv)
    next = fed.getNextGrantedTime()
    print("Next Time =" ,next)

# # # Executing finish #
fed.destroy()
print("Federate finalized")
h.helicsCloseLibrary()